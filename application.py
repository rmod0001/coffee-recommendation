from flask import Flask, render_template, redirect, url_for, request
import pickle
import joblib
import pandas as pd
import numpy as np

app = Flask(__name__)

global svm_model, lr_model, knn_model, decision_tree_model, random_forest_model
svm_model = joblib.load('models/svm_model.pkl')
lr_model = joblib.load('models/lr_model.pkl')
knn_model = joblib.load('models/knn_model.pkl')
decision_tree_model = joblib.load('models/decision_tree_model.pkl')
random_forest_model = joblib.load('models/random_forest_model.pkl')


def preprocessing(data_arabica, data_robusta):
    data_robusta.rename(
        {
            'Fragrance...Aroma': 'Aroma',
            'Salt...Acid': 'Acidity',
            'Bitter...Sweet': 'Sweetness',
            'Mouthfeel': 'Body',
            'Uniform.Cup': 'Uniformity'
        },
        axis=1, inplace=True)

    data = pd.concat([data_arabica, data_robusta], axis=0)

    df = data[
        ['Species',
         'Variety']]
    df = df[df['Variety'] != 'Other']
    df = df.dropna()
    df = df.reset_index()
    df = df.drop("index", axis=1)
    return df


@app.route('/', methods=['GET', 'POST'])
def index():
    # K.clear_session()
    return render_template('home.html')


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    global svm_model, lr_model, knn_model, decision_tree_model, random_forest_model
    if request.method == 'POST':

        model = str(request.form['models'])
        if model == 'svm':
            model_load = svm_model
        elif model == 'logistic_regression':
            model_load = lr_model
        elif model == 'knn':
            model_load = knn_model
        elif model == 'decision_tree':
            model_load = decision_tree_model
        else:
            model_load = random_forest_model

        aroma = int(request.form['aroma'])
        flavour = int(request.form['flavour'])
        aftertaste = int(request.form['aftertaste'])
        Acidity = int(request.form['Acidity'])
        Body = int(request.form['Body'])
        Balance = int(request.form['Balance'])
        Uniformity = int(request.form['Uniformity'])
        Clean_Cup = int(request.form['Clean_Cup'])
        Sweetness = int(request.form['Sweetness'])
        Cupper_Points = int(request.form['Cupper_Points'])
        Green_bean_grade = int(request.form['Green_bean_grade'])

        svm_prob = svm_model.predict_proba(
            [[aroma, flavour, aftertaste, Acidity, Body, Balance, Uniformity, Clean_Cup, Sweetness, Cupper_Points, Green_bean_grade]])
        lr_prob = lr_model.predict_proba([[aroma, flavour, aftertaste, Acidity, Body,
                                           Balance, Uniformity, Clean_Cup, Sweetness, Cupper_Points, Green_bean_grade]])
        knn_prob = knn_model.predict_proba(
            [[aroma, flavour, aftertaste, Acidity, Body, Balance, Uniformity, Clean_Cup, Sweetness, Cupper_Points, Green_bean_grade]])
        decision_tree_prob = decision_tree_model.predict_proba(
            [[aroma, flavour, aftertaste, Acidity, Body, Balance, Uniformity, Clean_Cup, Sweetness, Cupper_Points, Green_bean_grade]])
        # random_forest_prob = random_forest_model.predict_proba([[aroma,flavour,aftertaste,Acidity,Body,Balance,Uniformity,Clean_Cup,Sweetness,Cupper_Points,Green_bean_grade]])[:, 1]
        output = model_load.predict([[aroma, flavour, aftertaste, Acidity, Body, Balance,
                                      Uniformity, Clean_Cup, Sweetness, Cupper_Points, Green_bean_grade]])

        specie = ['Arabica', 'Robusta']
        legend = 'Accuracy Comparision'
        labels = ['SVM', "Logistic Regression", "KNN",
                  "Decision Tree"]

        data_arabica = pd.read_csv("./data/arabica_data_cleaned.csv")
        data_robusta = pd.read_csv("./data/robusta_data_cleaned.csv")
        dataframe = preprocessing(data_arabica, data_robusta)

        category = specie[output[0]]

        variety = dataframe[dataframe['Species']
                            == category]['Variety'].unique()

        values = [max(svm_prob[0]), max(lr_prob[0]), max(
            knn_prob[0]), max(decision_tree_prob[0])]

        print(category)
    return render_template('output.html', model_name=model, category=category, values=values, labels=labels, legend=legend, variety=variety)


if __name__ == '__main__':
    app.run(debug=True)
